#!/usr/bin/env ruby

# Va utiliser sinatra
require 'sinatra'

# Définit la route '/' et répond ... 
get '/' do 
  "Hello server v0.1.0\n" +
  "Please try /hello/something"
end 

# Définit la route '/hello/X' et répond 
# en fonction de X
get '/hello/:name' do 
  "Hello " + params[:name]
end 

